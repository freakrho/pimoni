import os


SAMPLE_INTERVAL = "SAMPLE_INTERVAL"
PORT = "PORT"


class Config:
    def __init__(self):
        # Defaults
        self.sample_interval = 2  # seconds
        self.port = 9090

    @staticmethod
    def get(ty, key: str, default):
        if key in os.environ:
            return ty(os.environ[key])
        return default

    def load(self):
        self.sample_interval = self.get(float, SAMPLE_INTERVAL, self.sample_interval)
        self.port = self.get(int, PORT, self.port)
