FROM python:3

RUN mkdir /usr/src/pimoni
WORKDIR /usr/src/pimoni

COPY requirements.txt .
COPY main.py .
COPY config.py .

RUN pip install -r requirements.txt

CMD [ "python", "main.py" ]