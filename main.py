from prometheus_client import start_http_server, Summary
import re
import subprocess
import time

# Create a metric to track time spent and requests made.
from config import Config

REQUEST_CPU_TEMPERATURE = Summary('request_cpu_temperature', 'CPU Temperature')
TEMP_COMMAND = "vcgencmd measure_temp"
TEMP_RE = "^temp=(.*)'C$"


# Decorate function with metric.
def check_temperature():
    print("Checking temperature...")
    try:
        output = subprocess.run(TEMP_COMMAND)
    except FileNotFoundError:
        output = "temp=37.0'C"
    match = re.match(TEMP_RE, output)
    print(f"Read {output}")
    for m in match.groups():
        print(float(m))
        REQUEST_CPU_TEMPERATURE.observe(float(m))


if __name__ == '__main__':
    print("Starting pimoni")

    config = Config()

    print(f"Starting server in port {config.port}")
    start_http_server(config.port)

    while True:
        check_temperature()
        time.sleep(config.sample_interval)
